def rot13(message):
    abc='abcdefghijklmnopqrstuvwxyz'
    ABC='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    out=''
    for i in message:
        if i in abc:
            out=out+str(abc[(abc.find(i)+13)%26])
        elif i in ABC:
            out=out+str(ABC[(ABC.find(i)+13)%26])
        else:
            out=out+i
    return out