def unique_in_order(iterable):
    out=[]
    for i in range(len(iterable)):
        if iterable[i]!=iterable[i-1] or i==0:
            out.append(iterable[i])
    return out