def DNA_strand(dna): #'T' and 'A' are complements of each other, as 'C' and 'G' also. Function return compement DNA
    b=''
    for i in dna:
        if i=='T':
            b+='A'
        elif i=='A':
            b+='T'
        elif i=='C':
            b+='G'
        elif i=='G':
            b+='C'
        else:
            return print('wrong DNA, try againg')
    return b
            
print(DNA_strand('CGGT'))