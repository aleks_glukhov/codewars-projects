def is_square(n):
    if n<0:
        return False
    elif (n**(1/2))%1==0 or n**(1/2)==0:
        return True
    else:
        return False