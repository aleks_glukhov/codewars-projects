def bouncing_ball(h, bounce, window):
    if h>0 and bounce>0 and bounce<1 and window<h:
        count=0
        h=h*bounce
        while h>window:
            h=h*bounce
            count+=2
        count+=1
        return count
    else:
        return -1