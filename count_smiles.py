def count_smileys(arr):
    eyes=(':', ';')
    nose=('-','~')
    mouse=(')','D')
    count=0
    for i in arr:
        if i[0] in eyes and (i[1] in nose and i[2] in mouse) or i[1] in mouse:
            count+=1
    return count