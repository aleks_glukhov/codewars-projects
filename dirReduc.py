'''… a man was given directions to go from one point to another.
 The directions were "NORTH", "SOUTH", "WEST", "EAST". 
 Clearly "NORTH" and "SOUTH" are opposite, "WEST" and "EAST" too.

Going to one direction and coming back the opposite direction
right away is a needless effort. Since this is the wild west,
 with dreadfull weather and not much water, 
 t's important to save yourself some energy, 
 otherwise you might die of thirst!'''



opposite = {'NORTH': 'SOUTH', 'EAST': 'WEST', 'SOUTH': 'NORTH', 'WEST': 'EAST'}
def dirReduc(arr):
    new_arr = []
    for i in arr:
        if new_arr and new_arr[-1] == opposite[i]:
            new_arr.pop()
        else:
            new_arr.append(i)
    return new_arr