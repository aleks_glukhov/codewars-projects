def sort_array(source_array):
    odd_array=[]
    for i in source_array:
        if i%2!=0:
            odd_array.append(i)
    odd_array.sort(reverse=True)
    for i in range(len(source_array)):
        if source_array[i]%2!=0:
            source_array[i]=odd_array.pop()
    return source_array